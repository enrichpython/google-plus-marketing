# Google Plus Marketing



## Development Guide
### Environment
1. Let's create a virtual environment (also called a virtualenv)
    ```
    $ python -m venv myvenv
    ```
2. Activate your virtual environment using a shell command:
    
    bash/zsh
    ```
    $ source <venv>/bin/activate
    ```
    Windows
    ```
    C:> <venv>/Scripts/activate.bat
    ```
    For powershell
    ```
    PS C:> <venv>/Scripts/Activate.ps1
    ```

3. Install requirements
    ```
    pip install -r requirements.txt
    ```
    
4. Run Flask server
    ```
    flask run
    ```